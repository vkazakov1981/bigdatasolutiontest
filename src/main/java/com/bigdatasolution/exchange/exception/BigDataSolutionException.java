package com.bigdatasolution.exchange.exception;

public class BigDataSolutionException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public BigDataSolutionException (String message)
	{
		super(message);
	}
}
