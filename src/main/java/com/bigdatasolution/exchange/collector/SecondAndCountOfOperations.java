package com.bigdatasolution.exchange.collector;

public class SecondAndCountOfOperations {
	private final long timeStamp;//timestamp
	private final int count;//operations count
	public SecondAndCountOfOperations(long timeStamp, int count)
	{
		this.timeStamp = timeStamp;
		this.count = count;
	}
	
	public long getTimeStamp() {
		return timeStamp;
	}
	
	public long getCount() {
		return count;
	}

	@Override
	public String toString() {
		return "SecondAndCountOfOperations [timeStamp=" + timeStamp + ", count=" + count + "]";
	}
}
