package com.bigdatasolution.exchange.collector;

import java.util.List;

import com.bigdatasolution.exchange.ExchangeInfo;

public class CollectorByTime {
	
	private final Long interval; //time unit [ms]

	public CollectorByTime(long interval) {
		this.interval = interval;
	}

	/**
	 * Result in [ms].
	 * 
	 * @return long, time stamp in [ms].
	 */
	public SecondAndCountOfOperations findASecondOfOperations(List<ExchangeInfo> exchangeInfoList) {
		//may be we need sort the list if the list is not sorted.		
		long resultSecondOfMaxOperation = 0L;
		int resultMaxOperation = 0;
		for(int i = 0 ; i < exchangeInfoList.size(); i++)
		{
		    int numOperations = 0;
		    long currentSecondOfMaxOperation = exchangeInfoList.get(i).getTimeStamp();		    
		    for(int ii = i+1 ; ii < exchangeInfoList.size(); ii++)
			{
		    	Long tailSecond = exchangeInfoList.get(ii).getTimeStamp();

		    	if((currentSecondOfMaxOperation + interval) > tailSecond)
		    	{
		    		numOperations++;
		    	} else {
		    		break;
		    	}
			}
		    if(resultMaxOperation < numOperations)
		    {
		    	resultMaxOperation = numOperations;
		    	resultSecondOfMaxOperation = currentSecondOfMaxOperation; 
		    }		    
		}
		
		return new SecondAndCountOfOperations(resultSecondOfMaxOperation, resultMaxOperation);
	}
}
