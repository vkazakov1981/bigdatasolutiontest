package com.bigdatasolution.exchange.collector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bigdatasolution.exchange.ExchangeInfo;

public class CollectorByExchangeAndTime {

	private final Long interval; // time unit [ms]

	public CollectorByExchangeAndTime(Long inteval) {
		this.interval = inteval;
	}

	/**
	 * Result in [ms].
	 * 
	 * @return long, time stamp in [ms].
	 */
	public Map<String, SecondAndCountOfOperations> findASecondOfOperations(List<ExchangeInfo> exchangeInfoList) {
		Map<String, SecondAndCountOfOperations > resultMap = new HashMap<>();
		
		Map<String, List<ExchangeInfo>> exchangeAndExchangeInfoMap = new HashMap<>();
		
		for (ExchangeInfo exchangeInfo : exchangeInfoList) {
			exchangeAndExchangeInfoMap.computeIfAbsent(exchangeInfo.getExchangeName(), e-> new ArrayList<>()).add(exchangeInfo);
		}
	
		for (Map.Entry<String, List<ExchangeInfo>> entry : exchangeAndExchangeInfoMap.entrySet()) {
			CollectorByTime collector = new CollectorByTime(interval);
			SecondAndCountOfOperations resultSecondWindow = collector.findASecondOfOperations(entry.getValue());
			resultMap.computeIfAbsent(entry.getKey(), e -> resultSecondWindow);
		}
		
		return resultMap;
	}
}
