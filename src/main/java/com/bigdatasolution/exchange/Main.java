package com.bigdatasolution.exchange;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import com.bigdatasolution.exchange.collector.CollectorByExchangeAndTime;
import com.bigdatasolution.exchange.collector.CollectorByTime;
import com.bigdatasolution.exchange.collector.SecondAndCountOfOperations;

public class Main {
	
	private static final Long TIME_INTERVAL_MS = 999L;
	private static final Logger LOG = Logger.getLogger(Main.class);
	private static final CollectorByTime collectorByTime = new CollectorByTime(TIME_INTERVAL_MS);
	private static final CollectorByExchangeAndTime collectorByExchangeAndTime = new CollectorByExchangeAndTime(
			TIME_INTERVAL_MS);

	private Main() {

	}

	public static void main(String[] args) {
		LOG.info("main begin");
		BasicConfigurator.configure();
		DataReader dataReader = new DataReader();		
		List<ExchangeInfo> exchangeInfoList = dataReader.retriveData();
		
		//Collections.sort(exchangeInfoList);
		//Not clear from task, is the list should be sorted ?
		
		SecondAndCountOfOperations resultOperation = collectorByTime.findASecondOfOperations(exchangeInfoList);
		
		Map<String, SecondAndCountOfOperations> result = collectorByExchangeAndTime.findASecondOfOperations(exchangeInfoList);
		
		printResult(resultOperation, result);
	}
	
	private static void printResult(SecondAndCountOfOperations resultOperation, Map<String, SecondAndCountOfOperations> result) {
		LOG.info("------------------------");
				
		StringBuilder sb = new StringBuilder(); 
		sb.append(formatTimeStamp(resultOperation.getTimeStamp()));
		sb.append("-");		
		sb.append(formatTimeStamp(resultOperation.getTimeStamp() + TIME_INTERVAL_MS));		
		LOG.info("Максимальное количество сделок в течение одной секунды было между " + sb.toString()
				+ ". В этот интервал прошло " + resultOperation.getCount() + " сделок.");
		LOG.info("------------------------");
		
		for (Map.Entry<String, SecondAndCountOfOperations> entry : result.entrySet()) {
			sb = new StringBuilder();			
			sb.append(formatTimeStamp(entry.getValue().getTimeStamp()));
			sb.append("-");	
			sb.append(formatTimeStamp(entry.getValue().getTimeStamp() + TIME_INTERVAL_MS));			
			LOG.info("Максимальное количество сделок в течение одной секунды было между " + sb.toString()
					+ ". В этот интервал прошло " + entry.getValue().getCount() + " сделок на бирже " + entry.getKey() + ".");
		}		
		LOG.info("------------------------");
		LOG.info("main end");
	}
	
	private static String formatTimeStamp(Long timeStamp) {
		final String timePattern = "%02d:%02d:%02d.%03d"; 
		Date date = new Date(timeStamp);		
		return String.format(timePattern, date.getHours(), date.getMinutes(),date.getSeconds(), timeStamp % 1000);		
	}
}
