package com.bigdatasolution.exchange;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import org.apache.log4j.Logger;

/**
 * Immutable exchange info
 */
public class ExchangeInfo implements Comparable<ExchangeInfo>{

	private static final Logger LOG = Logger.getLogger(ExchangeInfo.class);
	private static final SimpleDateFormat DATE_FORMATER = new SimpleDateFormat("HH:mm:ss.SSS");

	private final long timeStamp;
	private final double price;
	private final long shares;
	private final String exchangeName;
	
	/**
	 * Constructor
	 * 
	 * @param timeStamp the time stamp in [ms]
	 * @param price a price of one share
	 * @param shares amount of share in one operations
	 * @param exchageName
	 */

	public ExchangeInfo(long timeStamp, double price, int shares, String exchageName) {
		Objects.requireNonNull(exchageName);
		this.timeStamp = timeStamp;
		this.price = price;
		this.shares = shares;
		this.exchangeName = exchageName;
	}

	public static ExchangeInfo parse(String[] line) {
		Objects.requireNonNull(line);

		String stringTimeStamp = line[0];
		String stringPrice = line[1];
		String stringShares = line[2];
		String stringExchange = line[3];

		long timeStamp = 0L;
		Date date = null;
		try {
			date = DATE_FORMATER.parse(stringTimeStamp);
			timeStamp = date.getTime();
		} catch (ParseException e) {
			LOG.error("ParseException:" + stringTimeStamp, e);			
		}		
		double price = Double.parseDouble(stringPrice);
		int shares = Integer.parseInt(stringShares);

		return new ExchangeInfo(timeStamp, price, shares, stringExchange);
	}

	@Override
	public String toString() {
		return "ExchangeInfo [timeStamp=" + timeStamp + ", price=" + price + ", shares=" + shares + ", exchangeName="
				+ exchangeName + "]";
	}

	/**
	 * Get time step in [ms]
	 * 
	 * @return time stamp in [ms]
	 */
	public long getTimeStamp() {
		return timeStamp;
	}

	public double getPrice() {
		return price;
	}

	public long getShares() {
		return shares;
	}

	public String getExchangeName() {
		return exchangeName;
	}

	@Override
	public int compareTo(ExchangeInfo o) {
		if(this.timeStamp < o.getTimeStamp()) {
			return - 1;
		}
		if(this.timeStamp == o.getTimeStamp()) {
			return 0;
		}
		return 1;
		
	}

}
