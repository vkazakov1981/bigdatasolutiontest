package com.bigdatasolution.exchange;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.bigdatasolution.exchange.exception.BigDataSolutionException;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Data reader class is reading data line by like from resource line.
 */
public class DataReader {
	private static final Logger LOG = Logger.getLogger(DataReader.class);
	private static final String RESOURCE_FILE_NAME = "file/TRD.CSV";
	private List<ExchangeInfo> exchangeInfoList = null;

	public List<ExchangeInfo> retriveData() {
		if(exchangeInfoList != null) {
			return exchangeInfoList;
		}
		exchangeInfoList = new ArrayList<>();
		ClassLoader classloader = ClassLoader.getSystemClassLoader();
		URL url = classloader.getResource(RESOURCE_FILE_NAME);
		String fileResource = url.getFile();
		File file = new File(fileResource);		

		try (CSVReader reader = new CSVReader(new FileReader(file), ',')) {

			String[] line = reader.readNext();
			if (line == null) {
				LOG.error("no data in file");
				throw new BigDataSolutionException("The file is empty");
			}
			do {
				ExchangeInfo exchageInfo = ExchangeInfo.parse(line);
				exchangeInfoList.add(exchageInfo);

				line = reader.readNext();
			} while (line != null);
		} catch (IOException e) {
			LOG.error(e);
			throw new BigDataSolutionException("The CSV file contain erros");
		}
		return exchangeInfoList;
	}
}
